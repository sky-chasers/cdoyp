export const environment = {
    isProduction: false,
    baseUrl: import.meta.env['NG_APP_CDOYPFE_BASE_URL'],
    positionStackApiKey: import.meta.env['NG_APP_CDOYPFE_POSITIONSTACK_APIKEY'],
    positionStackBaseUrl: import.meta.env['NG_APP_CDOYPFE_POSITIONSTACK_BASE_URL'],
    geoApifyApiKey: import.meta.env['NG_APP_CEOYPFE_GEOAPIFY_API_KEY']
}