import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { LoginComponent } from './public-admin/login/login.component';
import { PublicAdminComponent } from './public-admin/public-admin.component';
import { AboutUsComponent } from './public/about-us/about-us.component';
import { BigMapComponent } from './public/big-map/big-map.component';
import { DestinationsComponent } from './public/destinations/destinations.component';
import { HomeComponent } from './public/home/home.component';
import { PublicComponent } from './public/public.component';
import { AuthGuard } from './common/authentication/authgaurd.service';


const routes: Routes = [
  { path: '', component: PublicComponent, children: [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'destinations', component: DestinationsComponent },
    { path: 'about-us', component: AboutUsComponent },
    { path: 'map', component: BigMapComponent }
  ]},
  { path: 'admin', redirectTo: '/admin/login', pathMatch: 'full' },
  { path: 'admin/login', component: PublicAdminComponent, children: [
    { path: '', component: LoginComponent }
  ]},
  { path: 'admin', component: AdminComponent, children: [
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] }
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
