import { NgModule } from '@angular/core';

import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PublicAdminModule } from './public-admin/public-admin.module';
import { PublicModule } from './public/public.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CsrfInterceptor } from './common/csrf/csrf.interceptor';
import { CsrfService } from './common/csrf/csrf.service';
import { AuthGuard } from './common/authentication/authgaurd.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AdminModule,
    PublicModule,
    PublicAdminModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    CsrfService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CsrfInterceptor,
      multi: true
    }],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
