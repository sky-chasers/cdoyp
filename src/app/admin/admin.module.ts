import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { AdminComponent } from './admin.component';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule
  ],
  providers: [
  ],
  exports: [
  ]
})
export class AdminModule { }
