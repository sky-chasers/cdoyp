export class LoginResponse {

    private id: string;
    private username: string;
    private csrfToken: string;

    constructor(id: string, username: string, csrfToken: string) {
        this.id = id;
        this.username = username;
        this.csrfToken = csrfToken;
    }

}