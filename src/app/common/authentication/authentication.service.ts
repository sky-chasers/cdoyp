import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as BuildUrl from 'build-url';
import { EnvironmentService } from '../environment/environment.service';
import { LoginResponse } from './login-response';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE_KEYS, LocalStorageService } from '../local-storage/local-storage.service';

@Injectable()
export class AuthenticationService {

    private loginPath:string = '/login';
    private environment: any;
    private userIsAuthenticated = false;

    constructor(
        environmentService: EnvironmentService,
        private httpClient: HttpClient,
        private localStorageService: LocalStorageService
    ) {
        this.environment = environmentService.getEnvironment();
    }

    public login(username: string, password: string): Observable<LoginResponse> {
        const fullUrl = BuildUrl(this.environment.baseUrl,{
            path: this.loginPath
        });

        const body = JSON.stringify({ username, password });

        return this.httpClient.post<LoginResponse>(fullUrl, body);
    }

    public setIsUserAuthenticated(isUserAuthenticated: boolean) {
        this.userIsAuthenticated = isUserAuthenticated; 
    }

    public isUserAuthenticated(): boolean {
        return this.userIsAuthenticated || this.localStorageService.getItem(LOCAL_STORAGE_KEYS.USER_INFO) !== null;
    }

}