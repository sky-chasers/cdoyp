import { Observable, Observer } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { LoginResponse } from './login-response';
import { LOCAL_STORAGE_KEYS } from '../local-storage/local-storage.service';

describe('AuthenticationService',() => {

    let service: AuthenticationService;
    let mockHttpClient: any;
    let mockLocalStorageService: any;
    let mockEnvironmentService: any;

    const mockEnvironment = {
        'baseUrl': 'baseUrl.com'
    };

    beforeEach(() => { 
        mockEnvironmentService = jasmine.createSpyObj('EnvironmentService', ['getEnvironment' ]);
        mockHttpClient = jasmine.createSpyObj('HttpClient', [ 'post' ]);
        mockLocalStorageService = jasmine.createSpyObj('LocalStorageService', [ 'getItem' ]);

        mockEnvironmentService
        .getEnvironment
        .and.returnValue(mockEnvironment);

        service = new AuthenticationService(
            mockEnvironmentService,
            mockHttpClient,
            mockLocalStorageService
        );
    });

    describe('login', () => {
        it('should make a POST request to the login endpoint with the correct username and password.', () => {
            const username = 'username';
            const password = 'password';

            const expectedBody = JSON.stringify({ username, password });
            const expectedUrl = 'baseUrl.com/login';
            const expectedLoginResponse = new LoginResponse('id', 'username', 'csrfToken');

            mockHttpClient.post
            .withArgs(expectedUrl, expectedBody)
            .and.returnValue(
                new Observable((observer: Observer<LoginResponse>) => {
                    observer.next(expectedLoginResponse);
                })
            );

            service.login(username, password)
            .subscribe(response => {
                expect(response).toBe(expectedLoginResponse);
            });
            
        });
    });

    describe('setIsUserAuthenticated', () => {
        it('should set the userIsAuthenticated to the correct value.', () => {
            service.setIsUserAuthenticated(true);
            expect(service.isUserAuthenticated()).toBeTrue();
        });
    });

    describe('isUserAuthenticated', () => {
        it('should return true if the user is authenticated.', () => {
            service.setIsUserAuthenticated(true);
            expect(service.isUserAuthenticated()).toBeTrue();
        });

        it('should return false if the user is not authenticated.', () => {
            service.setIsUserAuthenticated(false);

            mockLocalStorageService.getItem
            .withArgs(LOCAL_STORAGE_KEYS.USER_INFO)
            .and.returnValue(null);

            expect(service.isUserAuthenticated()).toBeFalse();
        });
    });

});