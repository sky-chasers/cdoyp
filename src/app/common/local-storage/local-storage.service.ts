import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    public getItem(key: string): any {
        const data = localStorage.getItem(key);
        return data ? JSON.parse(data) : null;
    }

    public setItem(key: string, value: any): void {
        localStorage.setItem(key, JSON.stringify(value));
    }

    public removeItem(key: string): void {
        localStorage.removeItem(key);
    }

    public clear(): void {
        localStorage.clear();
    }

}

export const LOCAL_STORAGE_KEYS = {
    'USER_INFO' : 'USER_INFO'
};