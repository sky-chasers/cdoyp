import { Injectable } from "@angular/core";
import { environment } from "src/environment/environment";

@Injectable()
export class EnvironmentService {

    public getEnvironment() {
        return environment;
    }

}
