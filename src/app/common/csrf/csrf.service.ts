import { Injectable } from '@angular/core';
import { EnvironmentService } from '../environment/environment.service';
import * as BuildUrl from 'build-url';

@Injectable()
export class CsrfService {
    
    private csrfPath = '/csrf';
    private csrfToken = '';
    private environment;

    constructor(
        private environmentService: EnvironmentService
    ) {
        this.environment = environmentService.getEnvironment();
    }

    public getCsrfToken() {
        return this.csrfToken;
    }

    public setCsrfToken(csrfToken: string) {
        this.csrfToken = csrfToken;
    }

    public async fetchCsrfToken() {
        const fullUrl = BuildUrl(this.environment.baseUrl,{
            path: this.csrfPath
        });

        const response = await fetch(fullUrl);
        const payload = await response.json();

        this.setCsrfToken(payload.token);
        return payload.token;
    }

}