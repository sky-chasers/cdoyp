import { Component, HostListener, Input } from '@angular/core';
import { FilterDropdownValue } from './filter-dropdown-value';

@Component({
    selector: 'app-filter-dropdown',
    templateUrl: './filter-dropdown.component.html',
    styleUrls: [
      './filter-dropdown.component.css',
      './filter-dropdown.mobile.component.css',
      './filter-dropdown.tablet.component.css'
    ]
})
export class FilterDropdownComponent {

    isDropdownVisible = false;

    @Input()
    values: FilterDropdownValue[] = [];

    public onCheckboxChange(value: FilterDropdownValue, event: any):void {
      value.isChecked = event.target.checked;
    }

    public closeDropdown():void {
      this.isDropdownVisible = false;
    }

    @HostListener('document:click', ['$event'])
    public onDocumentClick(event: MouseEvent) {
      const dropdownElement = document.querySelector('.dropdown');
      if(!dropdownElement?.contains(event.target as Node)) {
        this.closeDropdown();
      }
    }

}