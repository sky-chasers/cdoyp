import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FilterDropdownComponent } from './filter-dropdown.component';

@NgModule({
  declarations: [
    FilterDropdownComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
  ],
  exports: [ 
    FilterDropdownComponent
  ]
})
export class FilterDropdownModule { }
