export class FilterDropdownValue {

    public label: string;
    public isChecked: boolean;
    public value: any;

    constructor(label: string, value: any, isChecked: boolean) {
        this.label = label;
        this.isChecked = isChecked;
        this.value = value;
    }

}