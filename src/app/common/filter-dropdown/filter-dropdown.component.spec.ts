import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FilterDropdownComponent } from './filter-dropdown.component';
import { CommonModule } from '@angular/common';
import { FilterDropdownValue } from './filter-dropdown-value';
import { DestinationTypes } from '../destination/destination-types';
import { By } from '@angular/platform-browser';

describe('FilterDropdownComponent', () => {
    let component: FilterDropdownComponent;
    let fixture: ComponentFixture<FilterDropdownComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ 
                FilterDropdownComponent
            ],
            imports: [
                CommonModule
            ]
          })
        .compileComponents();
        
        fixture = TestBed.createComponent(FilterDropdownComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();

        component.values = [
            new FilterDropdownValue('Cafe', DestinationTypes.CAFE, false),
            new FilterDropdownValue('Resort', DestinationTypes.RESORT, true)
        ];
    });

    it('should display a filter button.', async() => {
        await fixture.whenStable();
        const filterButton = fixture.debugElement.queryAll(By.css('.dropdown'));

        expect(filterButton.length).toEqual(1);
    });

    it('should not display the labels of the filters when the dropdown is not visible.', async() => {
        await fixture.whenStable();
        
        const labels = fixture.debugElement.queryAll(By.css('.container'));

        expect(labels.length).toEqual(0);
    });

    it('should display the labels of the filters when the dropdown is visible.', async() => {
        component.isDropdownVisible = true;
        fixture.detectChanges();
        await fixture.whenStable();
        
        const labels = fixture.debugElement.queryAll(By.css('.container'));

        const labelTexts = labels.map((label) => label.nativeElement.textContent.trim());

        const expectedLabels = ['Cafe', 'Resort'];

        expect(labelTexts).toEqual(expectedLabels);
        expect(labels.length).toEqual(component.values.length);
    });

    describe('closeDropdown', () => {

        it('should close the dropdown when triggered.', () => {
            component.closeDropdown();

            expect(component.isDropdownVisible).toBeFalse();
        });

    });

    describe('onCheckboxChange', () => {

        it('should set the isChecked value to the correct value.', () => {
            const filterDropdownValue = new FilterDropdownValue('Restaurant', DestinationTypes.RESTAURANT, false);
            const event = { 'target': { 'checked' : true }}; 

            component.onCheckboxChange(filterDropdownValue, event);

            expect(filterDropdownValue.isChecked).toBeTrue();
        });

    });

    describe('onDocumentClick', () => {

        it('should close the dropdown when user clicks outside the bounderies of the component.', () => {
            const dummyElement = document.createElement('div');

            const mockMouseEvent = new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
                clientX: 100,
                clientY: 200
            });

            Object.defineProperty(mockMouseEvent, 'target', { value: dummyElement });

            component.onDocumentClick(mockMouseEvent);

            expect(component.isDropdownVisible).toBeFalse();
        });

    });

});  