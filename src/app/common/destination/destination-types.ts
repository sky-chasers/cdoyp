export enum DestinationTypes  {

    CAFE = 'Cafe',
    RESTAURANT = 'Restaurant',
    MALL = 'Mall',
    RESORT = 'Resort',
    PATISSERIE = 'Patiserrie'

}