import { Destination } from "./destination";

export interface PagedDestination {

    totalElements: number;
    size: number;
    totalPages: number;
    numberOfElements: number;
    totalElemets: number;
    number: number;
    first: boolean;
    last: boolean;
    empty: boolean;
    content: Destination[];

}