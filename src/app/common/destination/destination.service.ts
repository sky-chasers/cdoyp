import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as BuildUrl from 'build-url';
import { Observable } from 'rxjs';
import { EnvironmentService } from '../environment/environment.service';
import { PaginationUtils } from '../pagination/pagination-utils';
import { Destination } from './destination';
import { PagedDestination } from './paged-destination';

@Injectable()
export class DestinationService {

    destinationPath:string = '/destination';
    featuredDestinationPath:string = '/destination/featured';
    environment:any;

    constructor(
        private httpClient: HttpClient,
        private paginationUtils: PaginationUtils,
        private environmentService: EnvironmentService
    ) {
        this.environment = environmentService.getEnvironment();
    }

    public fetchAllDestinations(pageNumber: number, size: number): Observable<PagedDestination> {
        const backendPageNumber = this.paginationUtils.getPageNumberForBackend(pageNumber);

        const fullUrl = BuildUrl(this.environment.baseUrl,{
            path: this.destinationPath,
            queryParams: {
                page: backendPageNumber.toString(),
                size: size.toString()
            }
        });
        
        return this.httpClient.get<PagedDestination>(fullUrl);
    }

    public searchDestination(searchQuery: string, pageNumber: number, size: number): Observable<PagedDestination> {
        const backendPageNumber = this.paginationUtils.getPageNumberForBackend(pageNumber);

        const fullUrl = BuildUrl(this.environment.baseUrl,{
            path: this.destinationPath,
            queryParams: {
                page: backendPageNumber.toString(),
                size: size.toString(),
                query: searchQuery
            }
        });
        
        return this.httpClient.get<PagedDestination>(fullUrl);
    }

    public fetchFeaturedDestination(): Observable<Destination> {
        const fullUrl = BuildUrl(this.environment.baseUrl,{
            path: this.featuredDestinationPath
        });

        return this.httpClient.get<Destination>(fullUrl);
    }

}