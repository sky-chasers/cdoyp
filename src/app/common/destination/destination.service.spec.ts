import { Observable, Observer } from 'rxjs';
import { DestinationService } from './destination.service';
import { PagedDestination } from './paged-destination';
import { Destination } from './destination';

describe('DestinationService',() => {

    let service: DestinationService;
    let mockHttpClient: any;
    let mockPaginationUtils: any;
    let mockEnvironmentService: any;

    const mockEnvironment = {
        'baseUrl': 'baseUrl.com'
    };

    beforeEach(() => { 
        mockEnvironmentService = jasmine.createSpyObj('EnvironmentService', ['getEnvironment' ]);
        mockHttpClient = jasmine.createSpyObj('HttpClient', [ 'get' ]);
        mockPaginationUtils = jasmine.createSpyObj('PaginationUtils', [ 'getPageNumberForBackend' ]);

        mockEnvironmentService
        .getEnvironment
        .and.returnValue(mockEnvironment);

        service = new DestinationService(
            mockHttpClient,
            mockPaginationUtils,
            mockEnvironmentService
        );
    });

    describe('fetchAllDestinations', () => {

        it('should fetch all the destinations with the correct values.', () => {
            const pageNumber = 1;
            const size = 10;
            const expectedUrl = 'baseUrl.com/destination?page=0&size=10';
            const expectedPagedDestination = {} as PagedDestination;

            mockHttpClient.get
            .withArgs(expectedUrl)
            .and.returnValue(
                new Observable((observer: Observer<PagedDestination>) => {
                    observer.next(expectedPagedDestination);
                })
            );

            mockPaginationUtils.getPageNumberForBackend
            .withArgs(pageNumber)
            .and.returnValue(0);

            service.fetchAllDestinations(pageNumber, size)
            .subscribe(response => {
                expect(response).toBe(expectedPagedDestination);
            });
        });

    });

    describe('searchDestination', () => {

        it('should search all the destinations with the correct query and values.', () => {
            const pageNumber = 1;
            const size = 10;
            const searchQuery = 'fishbone';
            const expectedUrl = 'baseUrl.com/destination?page=0&size=10&query=fishbone';
            const expectedPagedDestination = {} as PagedDestination;

            mockHttpClient.get
            .withArgs(expectedUrl)
            .and.returnValue(
                new Observable((observer: Observer<PagedDestination>) => {
                    observer.next(expectedPagedDestination);
                })
            );

            mockPaginationUtils.getPageNumberForBackend
            .withArgs(pageNumber)
            .and.returnValue(0);

            service.searchDestination(searchQuery, pageNumber, size)
            .subscribe(response => {
                expect(response).toBe(expectedPagedDestination);
            });
        });

    });

    describe('fetchFeaturedDestination', () => {

        it('should fetch a random destination to be featured.', () => {
            const expectedUrl = 'baseUrl.com/destination/featured';
            const expectedDestination = {} as Destination;

            mockHttpClient.get
            .withArgs(expectedUrl)
            .and.returnValue(
                new Observable((observer: Observer<Destination>) => {
                    observer.next(expectedDestination);
                })
            );

            service.fetchFeaturedDestination()
            .subscribe(response => {
                expect(response).toBe(expectedDestination);
            });
        });

    });

});