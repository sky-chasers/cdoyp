import { DestinationTypes } from "./destination-types";

export interface Destination {

    id: string;
    name: string;
    address: string;
    phoneNumber: string;
    imageLink: string;
    businessLink: string;
    longitude: number;
    latitude: number;
    destinationType: DestinationTypes;

}