import { Injectable } from "@angular/core";
import { DestinationTypes } from "./destination-types";

@Injectable()
export class DestinationTypesService {

    iconMap = new Map<string, string>();

    constructor() {
        this.iconMap.set(DestinationTypes.CAFE, 'mug-hot');
        this.iconMap.set(DestinationTypes.RESTAURANT, 'utensils');
        this.iconMap.set(DestinationTypes.MALL, 'building');
        this.iconMap.set(DestinationTypes.RESORT, 'couch');
        this.iconMap.set(DestinationTypes.PATISSERIE, 'bread-slice');    
    }

    public getMapIcon(type : string) {
        const typeEnum = this.getEnumFromStringValue(DestinationTypes, type);
        return this.iconMap.get(typeEnum);
    }

    private getEnumFromStringValue<T extends string>(enumObject: { [key: string]: T }, value: string): T {
        return enumObject[value];
    }
      

}