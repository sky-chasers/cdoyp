import { Injectable } from "@angular/core";

@Injectable()
export class PaginationUtils {

    // Note: Backend starts it's page at page 0, not 1.
    // That's why this method exists.
    public getPageNumberForBackend(pageNumber: number): number {
        return pageNumber - 1;
    }

}