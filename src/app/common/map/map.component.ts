import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Map, map } from 'leaflet';
import { Destination } from '../destination/destination';
import { MapService } from './map.service';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: [
        './map.component.css',
        './map.mobile.component.css'
    ]
  })
  export class MapComponent implements AfterViewInit {
  
    @Input()
    destinations!: Destination[];

    @Input()
    initialZoom!: number;
    
    @ViewChild('map')
    mapContainer!: ElementRef<HTMLElement>;
    
    constructor(
        private mapService: MapService
    ) {
    }

    ngAfterViewInit() {
        const lefletMap: Map = map(this.mapContainer.nativeElement);
        this.mapService.setMap(lefletMap);
        this.mapService.setInitialZoom(this.initialZoom);
        this.mapService.addMarkersToMap(this.destinations);
    }

  }
  