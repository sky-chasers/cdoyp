import { Injectable } from '@angular/core';
import { Browser, Map, icon, layerGroup, map, marker, tileLayer } from 'leaflet';
import { Destination } from '../destination/destination';
import { DestinationTypes } from '../destination/destination-types';
import { DestinationTypesService } from '../destination/destination-types.services';
import { EnvironmentService } from '../environment/environment.service';

@Injectable()
export class MapService {

    private lefletMap: Map | undefined;
    private environment;
    private markersLayer: any = {};

    private DEFAULT_LONGITUDE = 124.6387164688;
    private DEFAULT_LATITUDE = 8.4845568628;

    private TILE_STYLE = 'positron';
    private ATTRIBUTION: string = "<a href='https://www.geoapify.com/' target='_blank'>Geoapify</a>";

    private initialZoom: number = 10;
    private maxZoom: number = 20;

    constructor(
        environmentService: EnvironmentService,
        private destinationTypesService: DestinationTypesService    
    ) {
        this.environment = environmentService.getEnvironment();
    }

    public setInitialZoom(initialZoom: number) {
        this.initialZoom = initialZoom;
    }

    public setMap(map: Map) {
        this.lefletMap = map;
    }

    public getMap() {
        console.log("getMap")
        return this.lefletMap;
    }

    public refreshMap() {
        /* Using invalidateSize refreshes the map so that it will load the appropriate tile size. */
        /* This is a fix to the bug where the tiles of map does not show completely due to the map being hidden in a modal. */
        window.dispatchEvent(new Event('resize'));
        this.lefletMap?.invalidateSize(true);
    }

    public addMarkersToMap(destinations: Destination[]) {
        this.markersLayer = {};

        if(destinations.length > 0) {
            for (let i = 0; i < destinations.length; i++) {
                this.createMarkerAndAddToMap(destinations[i]);
            }

            this.lefletMap?.setView([destinations[0].latitude, destinations[0].longitude], this.initialZoom);
        } else {
            this.lefletMap?.setView([this.DEFAULT_LATITUDE, this.DEFAULT_LONGITUDE], this.initialZoom);
        }

        if(this.lefletMap) {
            tileLayer(this.determineBaseUrl(), {
                attribution: this.ATTRIBUTION,
                apiKey: this.environment.geoApifyApiKey,
                maxZoom: this.maxZoom,
                id: this.TILE_STYLE,
            } as any).addTo(this.lefletMap);
        }

        Object.keys(DestinationTypes).forEach((destinationType) => {
            if(this.markersLayer[destinationType] !== undefined) {
                this.lefletMap?.addLayer(this.markersLayer[destinationType].layerGroup);
            }
        });

        this.refreshMap();
    }

    public hideMarkers(type: DestinationTypes) {
        if(this.markersLayer[type] !== undefined && this.markersLayer[type].isVisible) {
            this.lefletMap?.removeLayer(this.markersLayer[type].layerGroup);
            this.markersLayer[type].isVisible = false;
        }
    }

    public showMarkers(type: DestinationTypes) {   
        if(this.markersLayer[type] !== undefined) {
            if(!this.markersLayer[type].isVisible) {
                this.lefletMap?.addLayer(this.markersLayer[type].layerGroup);
                this.markersLayer[type].isVisible = true;    
            }
        }
    }


    private createMarkerAndAddToMap(destination: Destination) {
        const mapIcon = this.destinationTypesService.getMapIcon(destination.destinationType);
        
        const markerIcon = icon({
            iconUrl: `https://api.geoapify.com/v1/icon?size=xx-large&type=awesome&color=%233a3726&icon=${mapIcon}&apiKey=${this.environment.geoApifyApiKey}`,
            iconSize: [31, 46],
        });

        let mapMarker = marker([destination.latitude, destination.longitude], {
            icon: markerIcon
        });

        mapMarker.bindPopup('<a href="' + destination.businessLink +'" target="_blank" style="color:#3a3726; text-decoration: none; font-weight: bold;" >' + destination.name + '</a>');

        if(this.markersLayer[destination.destinationType] === undefined) {
            this.markersLayer[destination.destinationType] = {
                isVisible: true,
                layerGroup: layerGroup([])
            };
        }

        this.markersLayer[destination.destinationType].layerGroup.addLayer(mapMarker);
    }

    private determineBaseUrl():string {
        const isRetina = Browser.retina;
        const nonRetinaUrl = `https://maps.geoapify.com/v1/tile/${this.TILE_STYLE}/{z}/{x}/{y}.png?apiKey={apiKey}`;
        const retinaUrl = `https://maps.geoapify.com/v1/tile/${this.TILE_STYLE}/{z}/{x}/{y}@2x.png?apiKey={apiKey}`;

        return isRetina ? retinaUrl : nonRetinaUrl;
    }

}