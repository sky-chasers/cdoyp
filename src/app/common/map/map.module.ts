import { NgModule } from '@angular/core';
import { MapComponent } from './map.component';
import { CommonModule } from '@angular/common';
import { MapService } from './map.service';
import { DestinationTypesService } from '../destination/destination-types.services';

@NgModule({
  declarations: [
    MapComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    MapService,
    DestinationTypesService
  ],
  exports: [ 
    MapComponent
  ]
})
export class MapModule { }
