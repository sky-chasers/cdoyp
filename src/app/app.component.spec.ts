import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CsrfService } from './common/csrf/csrf.service';
import { AppRoutingModule } from './app-routing.module';

describe('AppComponent', () => {

  let mockCsrfTokenService: any = jasmine.createSpyObj('CsrfService', ['fetchCsrfToken']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        { provide: CsrfService, useValue: mockCsrfTokenService }
      ],
      declarations: [
        AppComponent
      ],
      imports: [
        AppRoutingModule
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

});
