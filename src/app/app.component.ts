import { Component, OnInit } from '@angular/core';
import { CsrfService } from './common/csrf/csrf.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css',
    './app.mobile.component.css',
    './app.tablet.component.css'
  ]
})
export class AppComponent implements OnInit {

  constructor(private headerService: CsrfService) {
  }

  async ngOnInit(): Promise<void> {
    await this.headerService.fetchCsrfToken();
  }

}
