import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { AuthenticationService } from 'src/app/common/authentication/authentication.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/common/local-storage/local-storage.service';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Observable, Observer, Subscription } from 'rxjs';
import { By } from '@angular/platform-browser';
import { LoginResponse } from 'src/app/common/authentication/login-response';

describe('LoginComponent',() => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    let mockAuthService:any = jasmine.createSpyObj('AuthenticationService', ['isUserAuthenticated', 'login', 'setIsUserAuthenticated']);
    let mockRouter:any = jasmine.createSpyObj('Router', ['navigate']);
    let mockLocalStorageService:any = jasmine.createSpyObj('LocalStorageService', ['setItem']);

    beforeEach(async () => {
        await TestBed.configureTestingModule({
          declarations: [ 
            LoginComponent
          ],
          providers: [
            { provide: AuthenticationService, useValue: mockAuthService },
            { provide: Router, useValue: mockRouter },
            { provide: LocalStorageService, useValue: mockLocalStorageService }
          ],
          imports: [
            ReactiveFormsModule,
            CommonModule
          ]
        })
        .compileComponents();
        
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it('should display the username text field.', () => {
        mockAuthService.isUserAuthenticated.and.returnValue(true);
        const usernameTextBox = fixture.debugElement.queryAll(By.css('.username input.textbox'));

        expect(usernameTextBox.length).toEqual(1);
    });

    it('should display the password text field.', () => {
        mockAuthService.isUserAuthenticated.and.returnValue(true);
        const passwordTextBox = fixture.debugElement.queryAll(By.css('.password input.textbox'));

        expect(passwordTextBox.length).toEqual(1);
    });

    it('should display the login button.', () => {
        mockAuthService.isUserAuthenticated.and.returnValue(true);
        const loginButton = fixture.debugElement.queryAll(By.css('.login-button input.button'));

        expect(loginButton.length).toEqual(1);
    });

    it('should display not display the error message initially.', () => {
        mockAuthService.isUserAuthenticated.and.returnValue(true);
        const errorMessage = fixture.debugElement.queryAll(By.css('.error-message'));

        expect(errorMessage.length).toEqual(0);
    });

    describe('ngOnDestroy', () => {
        it('should unsubscribe the login subscription.', () => {
            const mockLoginSubscription: jasmine.SpyObj<Subscription> = jasmine.createSpyObj('Subscription', ['unsubscribe']);

            component.loginSubscription = mockLoginSubscription;

            component.ngOnDestroy();

            expect(mockLoginSubscription.unsubscribe).toHaveBeenCalled();
        });
    });

    describe('ngOnInit', () => {
        it('should redirect to the dashboard if the user is authenticated.', () => {
            mockAuthService.isUserAuthenticated.and.returnValue(true);
            
            component.ngOnInit();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/dashboard']);
        });
    });

    describe('login', () => {

        it('should redirect to the dashboard if login is successful.', () => {
            const username = 'username';
            const password = 'password';

            component.loginForm = new FormGroup({
                username: new FormControl(username, [Validators.required]),
                password: new FormControl(password, [Validators.required])
            });

            const mockLoginResponse = new LoginResponse('id', username, 'csrfToken');
    
            mockAuthService.login
            .withArgs(username, password)
            .and.returnValue(
                new Observable((observer: Observer<LoginResponse>) => {
                    observer.next(mockLoginResponse);
                })
            );

            component.login();

            expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/dashboard']);
        });

        it('should not redirect to the dashboard if login failed.', () => {
            const username = 'username';
            const password = 'password';

            component.loginForm = new FormGroup({
                username: new FormControl(username, [Validators.required]),
                password: new FormControl(password, [Validators.required])
            });
    
            mockAuthService.login
            .withArgs(username, password)
            .and.returnValue(
                new Observable((observer: Observer<LoginResponse>) => {
                    observer.error('login failed.');
                })
            );

            component.login();
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                const errorMessage = fixture.debugElement.queryAll(By.css('.error-message'));

                expect(component.isErrorMessageVisible).toBeTrue();
                expect(component.isLoading).toBeFalse();
                expect(errorMessage.length).toEqual(1);
            });
        });
    });

});