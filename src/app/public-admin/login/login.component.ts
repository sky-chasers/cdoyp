import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/common/authentication/authentication.service';
import { LOCAL_STORAGE_KEYS, LocalStorageService } from 'src/app/common/local-storage/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css',
    './login.mobile.component.css'
  ]
})
export class LoginComponent implements OnInit, OnDestroy {

  isLoading = false;
  isErrorMessageVisible = false;
  errorMessage = 'Username or password is invalid.';
  loginSubscription: Subscription = new Subscription;
  
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  
  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private localStorageService: LocalStorageService
  ) { 
  }

  ngOnDestroy(): void {
    this.loginSubscription.unsubscribe();
  }

  ngOnInit() {
    if(this.authService.isUserAuthenticated()) {
      this.router.navigate(['/admin/dashboard']);
    }
  }

  public login() {
    this.isLoading = true;
    this.isErrorMessageVisible = false;

    const username = this.loginForm.get('username')?.value!;
    const password = this.loginForm.get('password')?.value!;

    this.loginSubscription = this.authService.login(username, password)
    .subscribe({
      next: (response) => {
        this.authService.setIsUserAuthenticated(true);
        this.router.navigate(['/admin/dashboard']);
        this.isLoading = false;
        this.localStorageService.setItem(LOCAL_STORAGE_KEYS.USER_INFO, response);
      },
      error: (error) => {
        this.isErrorMessageVisible = true;
        this.isLoading = false;
      }
    });
  }

}
