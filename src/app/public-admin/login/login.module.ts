import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from 'src/app/common/authentication/authentication.service';
import { LoginComponent } from './login.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [
    AuthenticationService
  ],
  exports: [
  ]
})
export class LoginModule { }
