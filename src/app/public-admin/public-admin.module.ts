import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { LoginModule } from './login/login.module';
import { PublicAdminComponent } from './public-admin.component';
import { LocalStorageService } from '../common/local-storage/local-storage.service';

@NgModule({
  declarations: [
    PublicAdminComponent
  ],
  imports: [
    BrowserModule,
    LoginModule,
    AppRoutingModule
  ],
  providers: [
    LocalStorageService
  ],
  exports: [
  ]
})
export class PublicAdminModule { }
