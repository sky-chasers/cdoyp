import { Component, OnInit } from '@angular/core';
import { Destination } from 'src/app/common/destination/destination';
import { DestinationTypes } from 'src/app/common/destination/destination-types';
import { DestinationService } from 'src/app/common/destination/destination.service';
import { FilterDropdownValue } from 'src/app/common/filter-dropdown/filter-dropdown-value';
import { MapService } from 'src/app/common/map/map.service';

@Component({
    selector: 'app-big-map',
    templateUrl: './big-map.component.html',
    styleUrls: [
      './big-map.component.css',
      './big-map.mobile.component.css',
      './big-map.tablet.component.css'
    ]
  })
  export class BigMapComponent implements OnInit {
  
  INITIAL_PAGE = 1;
  MAX_ITEMS = 1000;

  MAP_INITIAL_ZOOM = 13;

  destinations : Destination[] = [];
  destinationTypes: FilterDropdownValue[];
  
  constructor(
    private destinationService: DestinationService,
    private mapService: MapService
  ) {
    this.destinationTypes = Object.entries(DestinationTypes).map(([key, value]) => {
      return new FilterDropdownValue(value, key, true);
    });
  }

  public ngOnInit(): void {
    this.setupDestinations();
  }

  public filterMarkers() {
    const self = this;

    this.destinationTypes.forEach(function(type) {
      if(type.isChecked) {
        self.mapService.showMarkers(type.value);
      }else{
        self.mapService.hideMarkers(type.value);
      }
    });
  }

  private setupDestinations() {
    this.destinationService
    .fetchAllDestinations(this.INITIAL_PAGE, this.MAX_ITEMS)
    .subscribe(response => {
      this.destinations = response.content;
    });
  }
  
}