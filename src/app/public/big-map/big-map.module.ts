import { NgModule } from '@angular/core';
import { BigMapComponent } from './big-map.component';
import { CommonModule } from '@angular/common';
import { MapModule } from 'src/app/common/map/map.module';
import { FilterDropdownModule } from 'src/app/common/filter-dropdown/filter-dropdown.module';

@NgModule({
    declarations: [
        BigMapComponent
    ],
    imports: [
        MapModule,
        CommonModule,
        FilterDropdownModule
    ],
    providers: [
    ],
    exports: [ 
    ]
  })
  export class BigMapModule { }
  