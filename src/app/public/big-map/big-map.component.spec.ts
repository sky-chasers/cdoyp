import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, Observer } from 'rxjs';
import { BigMapComponent } from './big-map.component';
import { Destination } from 'src/app/common/destination/destination';
import { PagedDestination } from 'src/app/common/destination/paged-destination';
import { DestinationService } from 'src/app/common/destination/destination.service';
import { MapService } from 'src/app/common/map/map.service';
import { MapModule } from 'src/app/common/map/map.module';
import { FilterDropdownModule } from 'src/app/common/filter-dropdown/filter-dropdown.module';

describe('BigMapComponent',() => {

    let component: BigMapComponent;
    let fixture: ComponentFixture<BigMapComponent>;

    let mockDestinationService: any = jasmine.createSpyObj('DestinationService', ['fetchAllDestinations']);
    let mockMapService: any = jasmine.createSpyObj('MapService', ['showMarkers', 'hideMarkers', 'setMap', 'setInitialZoom', 'addMarkersToMap']);

    const mockDestination = {
        name: 'Bakery',
        address: 'some address',
        phoneNumber: '12345566',
        imageLink: 'imageLink.jpg',
        businessLink: 'businessLink.com',
        longitude: 120.33123,
        latitude: 38.4004
      } as Destination;
    
      const mockDestinations = {
        totalElements: 10,
        size: 1,
        totalPages: 1,
        numberOfElements: 1,
        number: 1,
        first: true,
        last: true,
        empty: false,
        content: [ mockDestination ]
      } as PagedDestination;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
          declarations: [ 
            BigMapComponent
          ],
          providers: [
            { provide: DestinationService, useValue: mockDestinationService },
            { provide: MapService, useValue: mockMapService }
          ],
          imports: [
            MapModule,
            CommonModule,
            FilterDropdownModule
          ]
        })
        .compileComponents();
        
        fixture = TestBed.createComponent(BigMapComponent);
        component = fixture.componentInstance;
        
        mockDestinationService.fetchAllDestinations
        .withArgs(component.INITIAL_PAGE, component.MAX_ITEMS)
        .and.returnValue(
          new Observable((observer: Observer<PagedDestination>) => {
            observer.next(mockDestinations);
          })
        );

        fixture.detectChanges();
    });

    it('should fetch a list of destinations when initialized.', fakeAsync(() => {
        fixture.detectChanges();
        expect(component.destinations).toEqual([mockDestination]);
    }));

    it('should initialize a list of destination types for the filter', () => {
        fixture.detectChanges();
        expect(component.destinationTypes.length).toEqual(5);

        expect(component.destinationTypes[0].value).toEqual('CAFE');
        expect(component.destinationTypes[0].isChecked).toEqual(true);
        expect(component.destinationTypes[0].label).toEqual('Cafe');

        expect(component.destinationTypes[1].value).toEqual('RESTAURANT');
        expect(component.destinationTypes[1].isChecked).toEqual(true);
        expect(component.destinationTypes[1].label).toEqual('Restaurant');

        expect(component.destinationTypes[2].value).toEqual('MALL');
        expect(component.destinationTypes[2].isChecked).toEqual(true);
        expect(component.destinationTypes[2].label).toEqual('Mall');

        expect(component.destinationTypes[3].value).toEqual('RESORT');
        expect(component.destinationTypes[3].isChecked).toEqual(true);
        expect(component.destinationTypes[3].label).toEqual('Resort');

        expect(component.destinationTypes[4].value).toEqual('PATISSERIE');
        expect(component.destinationTypes[4].isChecked).toEqual(true);
        expect(component.destinationTypes[4].label).toEqual('Patiserrie');
    });

    it('should create a big map with all the markers in it when initialized.', () => {
        fixture.detectChanges();

        const bigMap = fixture.debugElement.queryAll(By.css('.map'));
        expect(bigMap.length).toEqual(1);
    });

    it('should create a filter', () => {
        fixture.detectChanges();

        const filter = fixture.debugElement.queryAll(By.css('.destination-filter'));
        expect(filter.length).toEqual(1);
    });

    describe('filterMarkers', () => {

        it('should show the marker if the filter for the destination type is checked.', () => {
            component.destinationTypes[0].isChecked = false;
            component.destinationTypes[1].isChecked = true;
            component.destinationTypes[2].isChecked = false;
            component.destinationTypes[3].isChecked = true;
            component.destinationTypes[4].isChecked = false;

            component.filterMarkers();

            expect(mockMapService.showMarkers).toHaveBeenCalledWith(component.destinationTypes[1].value);
            expect(mockMapService.showMarkers).toHaveBeenCalledWith(component.destinationTypes[3].value);
            expect(mockMapService.hideMarkers).toHaveBeenCalledWith(component.destinationTypes[0].value);
            expect(mockMapService.hideMarkers).toHaveBeenCalledWith(component.destinationTypes[2].value);
            expect(mockMapService.hideMarkers).toHaveBeenCalledWith(component.destinationTypes[4].value);
        });

    });

});