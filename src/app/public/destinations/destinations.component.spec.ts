import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { Observable, Observer } from 'rxjs';
import { DestinationBoxComponent } from './destination-box/destination-box.component';
import { DestinationsComponent } from './destinations.component';
import { Destination } from 'src/app/common/destination/destination';
import { PagedDestination } from 'src/app/common/destination/paged-destination';
import { ModalModule } from 'src/app/common/modal/modal.module';
import { MapModule } from 'src/app/common/map/map.module';
import { EnvironmentService } from 'src/app/common/environment/environment.service';
import { PaginationUtils } from 'src/app/common/pagination/pagination-utils';
import { DestinationService } from 'src/app/common/destination/destination.service';

describe('DestinationsComponent', () => {
  let component: DestinationsComponent;
  let fixture: ComponentFixture<DestinationsComponent>;
  let mockDestinationService: any = jasmine.createSpyObj('DestinationService', ['fetchAllDestinations', 'searchDestination']);

  const INITIAL_PAGE_NUMBER = 1;

  const mockDestination = {
    name: 'Bakery',
    address: 'some address',
    phoneNumber: '12345566',
    imageLink: 'imageLink.jpg',
    businessLink: 'businessLink.com',
    longitude: 120.33123,
    latitude: 38.4004
  } as Destination;

  const mockDestinations = {
    totalElements: 10,
    size: 1,
    totalPages: 1,
    numberOfElements: 1,
    number: 1,
    first: true,
    last: true,
    empty: false,
    content: [ mockDestination ]
  } as PagedDestination;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        DestinationsComponent,
        DestinationBoxComponent
      ],
      imports: [
        ReactiveFormsModule,
        BrowserModule,
        NgxPaginationModule,
        ModalModule,
        MapModule
      ],
      providers: [
        EnvironmentService,
        PaginationUtils,
        { provide: DestinationService, useValue: mockDestinationService },
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(DestinationsComponent);
    component = fixture.componentInstance;

    mockDestinationService.fetchAllDestinations
    .withArgs(INITIAL_PAGE_NUMBER, component.ITEMS_PER_PAGE)
    .and.returnValue(
      new Observable((observer: Observer<PagedDestination>) => {
        observer.next(mockDestinations);
      })
    );

    fixture.detectChanges();
  });

  it('should fetch a list of destinations when initialized.', fakeAsync(() => {
    fixture.detectChanges();
    expect(component.destinations).toEqual([mockDestination]);
  }));

  it('should display the pagination links.', fakeAsync(() => {
    fixture.detectChanges();

    const pageNumbers = fixture.debugElement
    .queryAll(By.css('.page-number'));
    
    expect(pageNumbers.length).toEqual(2);
  }));

  it('should go to the correct page when a page number is clicked.', fakeAsync(() => {
    fixture.detectChanges();

    const pageNumbers = fixture.debugElement
    .queryAll(By.css('.page-number'));

    const page2 = pageNumbers[1].parent;
    page2?.triggerEventHandler("click", null);

    expect(mockDestinationService.fetchAllDestinations).toHaveBeenCalledWith(2, component.ITEMS_PER_PAGE);
  }));

  it('should go to the correct page when a page number is clicked while there is a search query.', fakeAsync(() => {
    const searchQuery = 'Manukan ni Ken';
    component.searchForm.controls['searchQuery'].setValue(searchQuery);

    fixture.detectChanges();

    const pageNumbers = fixture.debugElement
    .queryAll(By.css('.page-number'));

    const page2 = pageNumbers[1].parent;
    page2?.triggerEventHandler('click', null);

    expect(mockDestinationService.searchDestination).toHaveBeenCalledWith(searchQuery, 2, component.ITEMS_PER_PAGE);
  }));

});
