import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { DestinationService } from 'src/app/common/destination/destination.service';
import { EnvironmentService } from 'src/app/common/environment/environment.service';
import { MapModule } from 'src/app/common/map/map.module';
import { ModalModule } from 'src/app/common/modal/modal.module';
import { PaginationUtils } from 'src/app/common/pagination/pagination-utils';
import { DestinationBoxComponent } from './destination-box/destination-box.component';
import { DestinationsComponent } from './destinations.component';

@NgModule({
  declarations: [
    DestinationsComponent,
    DestinationBoxComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    NgxPaginationModule,
    ModalModule,
    MapModule
  ],
  providers: [
    DestinationService,
    EnvironmentService,
    PaginationUtils
  ],
  exports: [ 
    DestinationsComponent,
    DestinationBoxComponent
  ]
})
export class DestinationsModule { }
