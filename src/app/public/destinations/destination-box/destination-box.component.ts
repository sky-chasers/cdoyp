import { Component, Input, OnInit } from '@angular/core';
import { Destination } from 'src/app/common/destination/destination';
import { ModalService } from 'src/app/common/modal/modal.service';

@Component({
  selector: 'app-destination-box',
  templateUrl: './destination-box.component.html',
  styleUrls: [
    './destination-box.component.css',
    './destination-box.mobile.component.css',
    './destination-box.tablet.component.css'
  ]
})
export class DestinationBoxComponent implements OnInit{
  
  @Input() destination!:Destination;
  isModalOpen: boolean = false;

  MAP_INITIAL_ZOOM = 20;

  constructor(
    private modalService: ModalService
  ) {}
  
  ngOnInit(): void {
  }

  public openAddressModal(): void {
    const modalId = 'map-' + this.destination.id;
    
    this.modalService.open(modalId)
    .subscribe(response => {
      this.isModalOpen = response;
    });    
  }
}
