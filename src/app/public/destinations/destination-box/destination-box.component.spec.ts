import { ComponentFixture, TestBed } from '@angular/core/testing';

import { By } from '@angular/platform-browser';
import { Destination } from 'src/app/common/destination/destination';
import { MapModule } from 'src/app/common/map/map.module';
import { ModalModule } from 'src/app/common/modal/modal.module';
import { DestinationBoxComponent } from './destination-box.component';
import { EnvironmentService } from 'src/app/common/environment/environment.service';
import { ModalService } from 'src/app/common/modal/modal.service';
import { Observable, Observer } from 'rxjs';

describe('DestinationBoxComponent', () => {
  let component: DestinationBoxComponent;
  let fixture: ComponentFixture<DestinationBoxComponent>;

  const mockDestination = {
    name: 'Bakery',
    address: 'some address',
    phoneNumber: '12345566',
    imageLink: 'imageLink.jpg',
    businessLink: 'businessLink.com',
    longitude: 120.33123,
    latitude: 38.4004
  } as Destination;

  let mockModalService: any = jasmine.createSpyObj('ModalService', ['open', 'remove', 'add']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        DestinationBoxComponent
      ],
      providers: [
        EnvironmentService,
        { provide: ModalService, useValue: mockModalService }
      ],
      imports: [
        MapModule,
        ModalModule
      ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DestinationBoxComponent);
    component = fixture.componentInstance;
    component.destination = mockDestination;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the name of the destination.', () => {
    fixture.detectChanges();

    const name = fixture.debugElement
    .query(By.css('.name a'));
    
    expect(name.nativeElement.textContent.trim()).toBe(mockDestination.name);
  });

  it('should have a name with a link to the business.', () => {
    fixture.detectChanges();

    const name = fixture.debugElement
    .query(By.css('.name a'));
    
    expect(name.nativeElement.getAttribute('href')).toBe(mockDestination.businessLink);
  });

  it('should display the address of the destination.', () => {
    fixture.detectChanges();

    const address = fixture.debugElement
    .query(By.css('.address'));
    
    expect(address.nativeElement.textContent.trim()).toBe(mockDestination.address);
  });

  it('should display the phonenumber of the destination.', () => {
    fixture.detectChanges();

    const phoneNumber = fixture.debugElement
    .query(By.css('.phone-number'));
    
    expect(phoneNumber.nativeElement.textContent.trim()).toBe(mockDestination.phoneNumber);
  });

  it('should display an image of the destination.', () => {
    fixture.detectChanges();

    const image = fixture.debugElement
    .query(By.css('.image img'));
    
    expect(image.nativeElement.getAttribute('src')).toBe(mockDestination.imageLink);
  });

  describe('openAddressModal', () => {

    it('should open a modal with the correct id.',() => {
      mockModalService.open.and.returnValue(
        new Observable((observer: Observer<boolean>) => {
          observer.next(true);
        })
      );

      component.openAddressModal();

      const modalId = 'map-' + mockDestination.id;
      expect(mockModalService.open).toHaveBeenCalledOnceWith(modalId);
    });

  });

});
