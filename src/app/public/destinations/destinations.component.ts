import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Destination } from 'src/app/common/destination/destination';
import { DestinationService } from 'src/app/common/destination/destination.service';
import { PagedDestination } from 'src/app/common/destination/paged-destination';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: [
    './destinations.component.css',
    './destinations.mobile.component.css',
    './destinations.tablet.component.css'
  ]
})
export class DestinationsComponent implements OnInit, OnDestroy {

  ITEMS_PER_PAGE = 5;

  searchForm = new FormGroup({
    searchQuery: new FormControl('')
  });

  subscription: Subscription | undefined;
  destinations: Destination[] = [];
  isPageReady: boolean = false;
  totalElements: number = 10;
  currentPage: number = 1;

  constructor(
    private destinationService: DestinationService
  ) {
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.goToPage(1);
  }

  public goToPage(pageNumber: number):void {
    this.isPageReady = false;
    const searchQuery = this.searchForm.get('searchQuery')?.value!;

    if(searchQuery?.length === 0) {
      this.subscription = this.destinationService
      .fetchAllDestinations(pageNumber, this.ITEMS_PER_PAGE)
      .subscribe(response => {
        this.setupPage(response, pageNumber);
      });
    } else {
      this.subscription = this.destinationService
      .searchDestination(searchQuery, pageNumber, this.ITEMS_PER_PAGE)
      .subscribe(response => {
        this.setupPage(response, pageNumber);
      });
    }
  }

  public onSearchSubmit():void {
    this.goToPage(1);
  }

  private setupPage(response: PagedDestination, pageNumber: number) {
    this.isPageReady = true;
    this.destinations = response.content;
    this.totalElements = response.totalElements;
    this.currentPage = pageNumber;
  }

}
