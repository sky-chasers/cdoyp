import { Component } from '@angular/core';

@Component({
  selector: 'app-fifth-section',
  templateUrl: './fifth-section.component.html',
  styleUrls: [
    './fifth-section.component.css',
    './fifth-section.mobile.component.css',
    './fifth-section.tablet.component.css'
  ]
})
export class FifthSectionComponent {

}
