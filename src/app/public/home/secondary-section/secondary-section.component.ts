import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Destination } from 'src/app/common/destination/destination';
import { DestinationService } from 'src/app/common/destination/destination.service';

@Component({
  selector: 'app-secondary-section',
  templateUrl: './secondary-section.component.html',
  styleUrls: [
    './secondary-section.component.css',
    './secondary-section.mobile.component.css',
    './secondary-section.tablet.component.css'
  ]
})
export class SecondarySectionComponent implements OnInit, OnDestroy {

  public featuredDestination: Destination | undefined;
  public featuredDestinationSubscription: Subscription | undefined;

  constructor(private destinationService: DestinationService) {
  }

  ngOnInit() {
    this.featuredDestinationSubscription = this.destinationService
    .fetchFeaturedDestination()
    .subscribe(response => {
      this.featuredDestination = response;
    });
  }

  ngOnDestroy() {
    this.featuredDestinationSubscription?.unsubscribe();
  }

}
