import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { SecondarySectionComponent } from './secondary-section.component';
import { HomeModule } from '../home.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { Observable, Observer } from 'rxjs';
import { Destination } from 'src/app/common/destination/destination';
import { DestinationService } from 'src/app/common/destination/destination.service';
import { DestinationsModule } from '../../destinations/destinations.module';
import { AboutUsModule } from '../../about-us/about-us.module';

describe('SecondarySectionComponent', () => {
  let component: SecondarySectionComponent;
  let fixture: ComponentFixture<SecondarySectionComponent>;

  let mockDestinationService: any = jasmine.createSpyObj('DestinationService', ['fetchFeaturedDestination']);

  const mockDestination = {
    name: 'Bakery',
    address: 'some address',
    phoneNumber: '12345566',
    imageLink: 'imageLink.jpg',
    businessLink: 'businessLink.com'
  } as Destination;

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [ SecondarySectionComponent ],
      providers: [ 
        { provide: DestinationService, useValue: mockDestinationService }
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        HomeModule,
        AboutUsModule,
        DestinationsModule
      ]
    })
    .compileComponents();

    mockDestinationService.fetchFeaturedDestination.and.returnValue(
      new Observable((observer: Observer<Destination>) => {
          observer.next(mockDestination);
      })
    );

    fixture = TestBed.createComponent(SecondarySectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch the featured destination from the destination service when initialized.', fakeAsync(() => {
    fixture.detectChanges();
    
    fixture.whenStable().then(() => {
      expect(component.featuredDestination).toBe(mockDestination);
    });

  }));

});
