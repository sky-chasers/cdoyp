import { Component } from '@angular/core';

@Component({
  selector: 'app-third-section',
  templateUrl: './third-section.component.html',
  styleUrls: [
    './third-section.component.css',
    './third-section.mobile.component.css'
  ]
})
export class ThirdSectionComponent {

}
