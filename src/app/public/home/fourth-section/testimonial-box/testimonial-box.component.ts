import { Component, Input } from '@angular/core';
import { Testimonial } from '../testimonial';

@Component({
  selector: 'app-testimonial-box',
  templateUrl: './testimonial-box.component.html',
  styleUrls: [
    './testimonial-box.component.css',
    './testimonial-box.mobile.component.css',
    './testimonial-box.tablet.component.css'
  ]
})
export class TestimonialBoxComponent {
  @Input() testimonial!:Testimonial;
}
