export class Testimonial {
    
    public name: string;
    public testimony: string;
    public rating: number;
    public title: string;
    public image: string;

    private starsRating:string[];

    constructor(name: string, testimony: string, rating: number, title: string, image: string) {
        this.name = name;
        this.testimony = testimony;
        this.rating = rating;
        this.title = title;
        this.image = image;
        this.starsRating = this.setStarsRating(rating); 
    }

    private setStarsRating(rating: number) {
        let starsRating = [];

        const whiteStars = 5 - rating;
        const yellowStars = rating;

        for (let i = 0; i < yellowStars; i++) {
            starsRating.push("yellow-star.png");
        }

        for(let i = 0; i < whiteStars; i++) {
            starsRating.push("white-star.png");
        }

        return starsRating;
    }

    public getStarsRating() {
        return this.starsRating;
    }
}