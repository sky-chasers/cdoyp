import { Component, OnInit } from '@angular/core';
import { Testimonial } from './testimonial';

@Component({
  selector: 'app-fourth-section',
  templateUrl: './fourth-section.component.html',
  styleUrls: [
    './fourth-section.component.css',
    './fourth-section.mobile.component.css',
    './fourth-section.tablet.component.css'
  ]
})
export class FourthSectionComponent implements OnInit {

  allTestimonials:Testimonial[]=[];
  testimonials:Testimonial[]=[];

  ngOnInit() {

    this.allTestimonials = [
      new Testimonial('Beck William', 'Chada is exactly what I needed to explore CDO! I did not even know that these places existed until I looked at the app!', 5, 'Chef', 'becky.png'),
      new Testimonial('Hayden Craig', 'I thought I will be lost without this app! I explored CDO without ever leaving my room.', 5, 'Entrepreneur', 'hayden.png'),
      new Testimonial('Samantha Lam', 'It\'s a nice little app for exploring the city. I personally have not been to a lot of these suggested places.', 4, 'Traveler', 'sam.png'),
      new Testimonial('Jenny Sosa', 'Chada is such a nice app! I love exploring the place through my computer, I did not have to go out.', 5, 'Average Mom', 'jenny.png'),
      new Testimonial('Eden Mayfair', 'This is a cool app! I had an epiphany while looking for the cafe of my choice. I suggest you try this out!', 3, 'Model', 'eden.png'),
      new Testimonial('Adam Broady', 'For the love of god! Must such app really exist? I can\'t put my finger on it! I love it!', 5, 'Vlogger', 'adam.png'),
      new Testimonial('Timothy Alister', 'My reality has been shattered. What a commendable app! I cannot wait to check out the new features!', 5, 'Wannabe Poet', 'timothy.png'),
    ]

    this.testimonials = this.getRandomTestimonials(3);

  }

  private getRandomTestimonials(length: number) {
    const shuffled = [...this.allTestimonials].sort(() => 0.5 - Math.random());

    return shuffled.slice(0, length);
  }

}
