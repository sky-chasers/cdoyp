import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { PrimarySectionComponent } from './primary-section/primary-section.component';
import { SecondarySectionComponent } from './secondary-section/secondary-section.component';
import { ThirdSectionComponent } from './third-section/third-section.component';
import { FourthSectionComponent } from './fourth-section/fourth-section.component';
import { BrowserModule } from '@angular/platform-browser';
import { FifthSectionComponent } from './fifth-section/fifth-section.component';
import { TestimonialBoxComponent } from './fourth-section/testimonial-box/testimonial-box.component';

@NgModule({
  declarations: [
    HomeComponent,
    PrimarySectionComponent,
    SecondarySectionComponent,
    ThirdSectionComponent,
    FourthSectionComponent,
    FifthSectionComponent,
    TestimonialBoxComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
  ],
  exports: [ 
    HomeComponent,
    TestimonialBoxComponent
  ]
})
export class HomeModule { }
