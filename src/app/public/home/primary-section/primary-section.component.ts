import { Component } from '@angular/core';

@Component({
  selector: 'app-primary-section',
  templateUrl: './primary-section.component.html',
  styleUrls: [
    './primary-section.component.css',
    './primary-section.mobile.component.css',
    './primary-section.tablet.component.css'
  ]
})
export class PrimarySectionComponent {

}
