import { Component } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: [
    './about-us.component.css',
    './about-us.mobile.component.css',
    './about-us.tablet.component.css'
  ]
})
export class AboutUsComponent {

}
