import { NgModule } from '@angular/core';
import { AboutUsComponent } from './about-us.component';

@NgModule({
  declarations: [
    AboutUsComponent
  ],
  imports: [
  ],
  providers: [],
  exports: [ 
    AboutUsComponent
  ]
})
export class AboutUsModule { }
