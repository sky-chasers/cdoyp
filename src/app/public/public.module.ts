import { NgModule } from '@angular/core';
import { PublicComponent } from './public.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { HomeModule } from './home/home.module';
import { DestinationsModule } from './destinations/destinations.module';
import { BigMapModule } from './big-map/big-map.module';
import { AboutUsModule } from './about-us/about-us.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    PublicComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    DestinationsModule,
    BigMapModule,
    AboutUsModule,
    HttpClientModule
  ],
  providers: [
  ],
  exports: [
  ]
})
export class PublicModule { }
