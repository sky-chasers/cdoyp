import { Component } from '@angular/core';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: [
    './public.component.css',
    './public.mobile.component.css',
    './public.tablet.component.css'
  ]
})
export class PublicComponent {
  title = 'Discover Cagayan de oro';
  currentYear = new Date().getFullYear();
}
